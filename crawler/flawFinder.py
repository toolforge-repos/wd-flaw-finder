import urllib.request
import sys
import os
import logging
import traceback
import random
import json
import pymysql
import wikidataItem
import config


# DB-Setup
# On Wikimedia: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Steps_to_create_a_user_database_on_tools.db.svc.wikimedia.cloud
"""
CREATE USER 'flawFinder'@'localhost' IDENTIFIED BY 'NDNlMTEwMjk3MDM4NmYxMzBjODk3MThj';
DROP DATABASE IF EXISTS wikidataFlawFinder;
CREATE DATABASE wikidataFlawFinder;
USE wikidataFlawFinder;

DROP TABLE IF EXISTS flaws , flawCategory;

CREATE TABLE flaws (
    flawID int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
    wikidataID int,
    flawCategoryID int,
    flawDescription varchar(512)
);

CREATE TABLE flawCategory (
    flawCategoryID int PRIMARY KEY,
    categoryName varchar(512),
    categoryDescription varchar(512)
);

CREATE TABLE flawWhitelist (
    wikidataID int,
    flawCategoryID int,
    whitelistReason varchar(512),
    PRIMARY KEY(wikidataID, flawCategoryID)
);

GRANT ALL PRIVILEGES ON wikidataFlawFinder.* TO 'flawFinder'@'localhost';

FLUSH PRIVILEGES;
"""

database_path = config.database_path
database_user = config.database_user
database_password = config.database_password
database_name = config.database_name


def empty_error_db():
    db = pymysql.connect(host=database_path, user=database_user, password=database_password, database=database_name )   # Open database connection
    cursor = db.cursor()    # prepare a cursor object using cursor() method

    cursor.execute("TRUNCATE flaws")
    ret = db.commit()
    print(cursor.rowcount, "TABLE truncated.")

    db.close()  # disconnect from server

def insert_entry_to_db(flawCategoryID, categoryName, categoryDescription, wikidataID, flawDescription):
    db = pymysql.connect(host=database_path, user=database_user, password=database_password, database=database_name )   # Open database connection
    cursor = db.cursor()    # prepare a cursor object using cursor() method

    # category
    cursor.execute("INSERT INTO flawCategory (flawCategoryID, categoryName, categoryDescription) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE flawCategoryID=%s, categoryDescription=%s", (flawCategoryID, categoryName, categoryDescription, flawCategoryID, categoryDescription) )
    ret = db.commit()
    print(cursor.rowcount, "record inserted.")
    # print ("Database returns: %s " % ret)

    # flaw
    cursor.execute("INSERT INTO flaws (wikidataID, flawCategoryID, flawDescription) VALUES (%s, %s, %s)", (wikidataID[1:], flawCategoryID, flawDescription) )
    ret = db.commit()
    print(cursor.rowcount, "record inserted.")

    db.close()  # disconnect from server


def func_001_not_part_of_series( wd_object ):
    # global wikidataID_of_episode_to_check

    if ("P179" in wd_object["claims"]):
        print("has_series")
    else:
        insert_entry_to_db("1", "No 'Parent'-Series found", "For these Entrys no 'Parent'-Series was found, although every television episode should have such a Series", wikidataID_of_episode_to_check, "No series (P179) found")


def func_002_no_season_or_ordinal( wd_object ):
    if ("P4908" in wd_object["claims"]):
        # print(json.dumps(wd_object["claims"]["P4908"], indent = 4))
        try:
            if ("P1545" in wd_object["claims"]["P4908"][0]["qualifiers"]):
                print("has_season_and_ordinal")
            else:
                insert_entry_to_db("2", "No Season found", "For these Entrys no Season (and/or Season ordinal) was found, although every television episode should have such a Season.", wikidataID_of_episode_to_check, "Season (P4908) found, but no Ordinal")
        except Exception as e:
            insert_entry_to_db("2", "No Season found","For these Entrys no Season (and/or Season ordinal) was found, although every television episode should have such a Season.", wikidataID_of_episode_to_check, "Season (P4908) found, but no Ordinal")
    else:
        insert_entry_to_db("2", "No Season found","For these Entrys no Season (and/or Season ordinal) was found, although every television episode should have such a Season.", wikidataID_of_episode_to_check, "No Season (P4908) found")


def func_003_no_follows( wd_object ):
    follows_found = 0
    comment = ""
    # print(json.dumps(wd_object["claims"], indent = 4))
    if ("P155" in wd_object["claims"]):
        follows_found = 1
    elif ("P179" in wd_object["claims"]):
        # print("has part of the series")
        # print(json.dumps(wd_object["claims"]["P179"][0], indent = 4))
        if ("qualifiers" in wd_object["claims"]["P179"][0]):
            # print("has qualifiers")
            if ("P155" in wd_object["claims"]["P179"][0]["qualifiers"]):
                follows_found = 1
            else:
                comment += ", no part of the series with qualifier follows found"
        else:
                comment += ", no part of the series with qualifier follows found"
    elif ("P4908" in wd_object["claims"]):
        # print("has part of the series")
        # print(json.dumps(wd_object["claims"]["P179"][0], indent = 4))
        if ("qualifiers" in wd_object["claims"]["P4908"][0]):
            # print("has qualifiers")
            if ("P155" in wd_object["claims"]["P4908"][0]["qualifiers"]):
                follows_found = 1
            else:
                comment += ", no season with qualifier follows found"
        else:
            comment += ", no season with qualifier follows found"
    else:
        comment += "no P155 as Claim"

    if (follows_found == 0):
        insert_entry_to_db("3", "No follows (P155) was found", "For these Entrys no claim/qualifier follows (P155) was found. All Entrys that are a instance of 'television series episode' should have a Value for follows, for the first Item of a Series, the Value 'No Value' should be set.", wikidataID_of_episode_to_check, "No follows (P155) found" + comment)


def func_004_ordinal_conflict_with_imdb( wd_object ):
    print("func_004_ordinal_conflict_with_imdb")

def func_005_no_title( wd_object ):
    print("func_005_has_title")


if (os.path.exists(os.path.dirname(__file__) + "/logs/episodes.log")):
    os.remove(os.path.dirname(__file__) + "/logs/episodes.log")

logging.basicConfig(filename=os.path.dirname(__file__) + "/logs/episodes.log", level=logging.DEBUG)

# Load fresh episodes on script start...
# os.remove("episodes.csv")
if (not os.path.exists("episodes.csv")):

    req = "https://query.wikidata.org/sparql?query=SELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20WHERE%20%7B%0A%20%20%7B%0A%20%20%20%20SELECT%20DISTINCT%20%3Fitem%20WHERE%20%7B%0A%20%20%20%20%20%20%3Fitem%20p%3AP31%20%3Fstatement0.%0A%20%20%20%20%20%20%3Fstatement0%20%28ps%3AP31%29%20wd%3AQ21191270.%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D"
    content = urllib.request.urlopen(req).read().decode('utf-8')

    file = open("tmp.txt","w")
    file.write(str(content))
    file.close()

    # Convert requested episodes in processable format
    good_words = ['<uri>']
    with open('tmp.txt') as oldfile, open('episodes.csv', 'w') as newfile:
        for line in oldfile:
            if any(good_word in line for good_word in good_words):
                newfile.write(line[9:-7]+"\n")
    os.remove("tmp.txt")


num_lines = sum(1 for line in open('episodes.csv'))

with open('episodes.csv', 'r') as fin:
	data = fin.read().splitlines(True)

empty_error_db() # Cleanup befor running

while (len(data) > 0) :
    try:
        print("################################################################")
        print("-----------------------------------------")
        print("| Number of Entrys (Beginning): "+str(num_lines)+"\t|")
        print("| Number of Entrys (Current):   "+str(len(data))+"\t|")
        print("-----------------------------------------")
        print("| Checked Entrys:               "+str(num_lines-len(data))+"\t|")
        print("-----------------------------------------")

        random_entry = random.randint(0,len(data)-1)
        episode_to_check = data[random_entry]
        del data[random_entry]

        print(episode_to_check)
        # print(episode_to_check.rsplit('/', 1))
        # print(episode_to_check.rsplit('/', 1)[-1])
        wikidataID_of_episode_to_check = episode_to_check.rsplit('/', 1)[-1]

        episodeToCheck = wikidataItem.wikidataItem(wikidataID_of_episode_to_check)

        # print(json.dumps(episodeToCheck.wikidata_object, indent = 4))

        func_001_not_part_of_series(episodeToCheck.wikidata_object)
        func_002_no_season_or_ordinal(episodeToCheck.wikidata_object)
        func_003_no_follows(episodeToCheck.wikidata_object)
        func_004_ordinal_conflict_with_imdb(episodeToCheck.wikidata_object)
        func_005_no_title(episodeToCheck.wikidata_object)

        # followed by/follow, but series ordinal has a differnence greater than one (+-1)



    except Exception as e:
        logging.debug("wikidataID_of_episode_to_check: " + wikidataID_of_episode_to_check + " | " + traceback.format_exc())

# os.remove("episodes.csv")
