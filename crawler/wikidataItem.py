import urllib.request
import json


class wikidataItem:

    def __init__(self, itemID):
        self.itemID = itemID
        self.loadItem()

    def loadItem(self):
        req = 'https://www.wikidata.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&rvslots=main&titles='+self.itemID
        # Overwrite
        # req = 'https://www.wikidata.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&rvslots=main&titles=Q3168447'

        self.content = urllib.request.urlopen(req).read().decode('utf-8')
        self.json_content = json.loads(self.content)

        pageID = list(self.json_content["query"]["pages"].keys())[0]
        self.wikidata_object = json.loads(self.json_content["query"]["pages"][list(self.json_content["query"]["pages"].keys())[0]]["revisions"][0]["slots"]["main"]["*"])
