#
# flask run --host=<ip/domain> --port=<port>
#

from flask import Flask, render_template, request, redirect
import pymysql
import json
import config

app = Flask(__name__)

database_path = config.database_path
database_user = config.database_user
database_password = config.database_password
database_name = config.database_name


@app.route('/')
def index():
    db = pymysql.connect(host=database_path, user=database_user, password=database_password, database=database_name )   # Open database connection
    cursor = db.cursor()    # prepare a cursor object using cursor() method

    cursor.execute("SELECT * FROM flawCategory")
    ret = db.commit()
    entrys = cursor.fetchall()
    
    i = 0
    entryCount = []
    while i < len(entrys):
        cursor.execute("SELECT COUNT(*) FROM flaws WHERE flawCategoryID = %d" % entrys[i][0])
        ret = db.commit()
        numberOfEntrys = cursor.fetchall()
        entryCount.append(numberOfEntrys[0][0])
        i += 1
        
    return render_template('index.html', len=len(entrys), entry=entrys, entryCounter=entryCount)


@app.route('/<view>/<categoryID>')
def category(view, categoryID):
    orderBy = request.args.get('orderby', default='wikidataID')           # ID / Description
    orderType = request.args.get('sort', default='ASC')            # ASC or DESC
    offset = request.args.get('offset', default='0')             # 25, 50, 69
    resolve = request.args.get('resolve')           # true/false
    whitelist = request.args.get('whitelist')       # true/false
    wikidataid = request.args.get('wikidataid')     # wikidataid
    whitelistReason = request.args.get('whitelistReason')   # whitelistReason
    # View Whitelist
    unlist = request.args.get('unlist')       # true/false
    # View Delta
    deltaresolve = request.args.get('deltaresolve')       # true/false


    db = pymysql.connect(host=database_path, user=database_user, password=database_password, database=database_name )   # Open database connection
    cursor = db.cursor()    # prepare a cursor object using cursor() method

    # Calculate Next Offset-Options
    if int(offset) < 25:
        offsetOptions = (0, int(offset), int(offset)+25)
    else:
        offsetOptions = (int(offset)-25, int(offset), int(offset)+25)
    
    # Get Info about the selected Category
    categoryInfo = ""
    try:
        cursor.execute("SELECT * FROM flawCategory WHERE flawCategoryID = %s" % categoryID)
        ret = db.commit()
        categoryInfo = cursor.fetchall()[0]
    except:
        # PRIMARY KEY Error: Already in the whitelist 
        pass
    
    # Order     ORDER BY wikidataID ASC LIMIT 25
    order = "ORDER BY %s %s" % (orderBy, orderType)
    # Limit and Offset
    limitOffset = "LIMIT 25 OFFSET %s" % (offset)

    # Process different Views (category/whitelist/delta)
    if view == "category":
        if whitelist == "true":
            try:
                cursor.execute("INSERT INTO flawWhitelist (wikidataID, flawCategoryID, whitelistReason) VALUES (%d, %d, '%s');" % (int(wikidataid), int(categoryID), whitelistReason))
                ret = db.commit()
            except:
                # PRIMARY KEY Error: Already in the whitelist 
                pass
        
        if resolve == "true":
            try:
                cursor.execute("DELETE FROM flaws WHERE wikidataID = %d AND flawCategoryID = %d;" % (int(wikidataid), int(categoryID)))
                ret = db.commit()
            except:
                # Trying to delete non existing Object
                pass

        cursor.execute("SELECT * FROM flaws WHERE flawCategoryID = '%s' AND NOT EXISTS (SELECT * FROM flawWhitelist WHERE wikidataID = flaws.wikidataID AND flawCategoryID = flaws.flawCategoryID) %s %s" % (categoryID, order, limitOffset))
        ret = db.commit()
        entrys = cursor.fetchall()
        db.close()  # disconnect from server

        if categoryID == "favicon.ico":
            return redirect("/favicon.ico", code=302)
        else:
            return render_template('category.html', pageContent="To-Do", categoryID=categoryID, categoryInfo=categoryInfo, len=len(entrys), entrys=entrys, offset=offset, orderby=orderBy, sort=orderType, offsetOptions=offsetOptions)
        
    elif view == "whitelist":
        if unlist == "true":
            try:
                cursor.execute("DELETE FROM flawWhitelist WHERE wikidataID=%d AND flawCategoryID=%d;" % (int(wikidataid), int(categoryID)))
                ret = db.commit()
            except Exception as e:
                pass
        
        cursor.execute("SELECT * FROM flawWhitelist WHERE flawCategoryID = '%s' %s %s" % (categoryID, order, limitOffset))
        ret = db.commit()
        entrys = cursor.fetchall()
        db.close()  # disconnect from server
            
        return render_template('category.html', pageContent="Whitelist", categoryID=categoryID, categoryInfo=categoryInfo, len=len(entrys), entrys=entrys, offset=offset, orderby=orderBy, sort=orderType, offsetOptions=offsetOptions)
        
    elif view == "delta":
        if deltaresolve == "true":
            try:
                cursor.execute("DELETE FROM flawWhitelist WHERE wikidataID=%d AND flawCategoryID=%d;" % (int(wikidataid), int(categoryID)))
                ret = db.commit()
            except Exception as e:
                pass

        cursor.execute("SELECT * FROM flawWhitelist WHERE flawCategoryID = '%s' AND NOT EXISTS (SELECT * FROM flaws WHERE wikidataID = flawWhitelist.wikidataID AND flawCategoryID = flawWhitelist.flawCategoryID) %s %s" % (categoryID, order, limitOffset))
        ret = db.commit()
        entrys = cursor.fetchall()
        db.close()  # disconnect from server
        
        return render_template('category.html', pageContent="Delta", categoryID=categoryID, categoryInfo=categoryInfo, len=len(entrys), entrys=entrys, offset=offset, orderby=orderBy, sort=orderType, offsetOptions=offsetOptions)

    else:
        return render_template('404.html')

